package com.example.laura.shifumi.shifumi.controllers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Laura on 28/01/2018.
 */

public class SendChoiceController implements Runnable {

    public MyChoiceController controller;
    public String action;

    public SendChoiceController (MyChoiceController controller) {
        this.controller = controller;
    }

    @Override
    public void run()
    {

        try {
            InetAddress address = (controller.shifumi.gameModel.getIsGroupOwner() ?  controller.shifumi.gameModel.getClientAddress() : controller.shifumi.gameModel.getOwnerAdress());

            Socket clientSocket = new Socket(address, MyChoiceController.SERVERPORT);

            String message = action;

            BufferedOutputStream bos = new BufferedOutputStream(clientSocket.getOutputStream());

            // on écrit le message
            bos.write(message.getBytes());
            bos.flush();

            if (clientSocket != null) {
                if (!clientSocket.isClosed()) {
                    try {
                        clientSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
                e.printStackTrace();
        }

        if (this.action.equals("init"))
            this.controller.initDone = true;

        this.controller.choiceTransfered();
    }

    public void setAction(String action) {
        this.action = action;
    }
}
