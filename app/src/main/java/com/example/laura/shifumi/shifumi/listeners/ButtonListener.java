package com.example.laura.shifumi.shifumi.listeners;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.net.wifi.p2p.WifiP2pDevice;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.laura.shifumi.shifumi.fragments.FragmentSelecteur;
import com.example.laura.shifumi.R;
import com.example.laura.shifumi.shifumi.fragments.HomeFragment;
import com.example.laura.shifumi.shifumi.fragments.JoinFragment;
import com.example.laura.shifumi.shifumi.fragments.ShifumiFragment;
import com.example.laura.shifumi.shifumi.model.GameModel;

import java.net.InetAddress;

/**
 * Created by Laura on 13/01/2018.
 */

public class ButtonListener implements Button.OnClickListener {

    FragmentSelecteur selecteur;
    WifiP2pDevice opponentDevice;
    private boolean isGroupOwner;
    private InetAddress ownerAdress;

    public ButtonListener(FragmentSelecteur selecteur)
    {
        this.selecteur = selecteur;
    }


    @Override
    public void onClick(View v) {

        Fragment newFragment;
        FragmentManager fragmentManager = this.selecteur.getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (v == this.selecteur.joinButton) {
            newFragment = new JoinFragment();
            ((JoinFragment)newFragment).setFragmentSelecteur(this.selecteur);
        } else if (v == this.selecteur.launchGameButton) {
            newFragment = new ShifumiFragment();
            ((ShifumiFragment) newFragment).setFragmentSelecteur(this.selecteur);
            this.selecteur.gameModel = new GameModel(this.opponentDevice, this.isGroupOwner, this.ownerAdress);
            ((ShifumiFragment) newFragment).setGameModel(this.selecteur.gameModel);
        } else if (v == this.selecteur.nextRoundButton) {
            newFragment = new ShifumiFragment();
            ((ShifumiFragment) newFragment).setGameModel(this.selecteur.gameModel);
            ((ShifumiFragment) newFragment).setFragmentSelecteur(this.selecteur);
        } else {
            newFragment = null;
        }

        if (newFragment != null) {
            this.selecteur.replaceFragment(newFragment);
        }

    }

    public void addOpponentDevice(WifiP2pDevice result) {
        this.opponentDevice = result;
    }

    public void addIsGroupOwner(boolean isGroupOwner) {
        this.isGroupOwner = isGroupOwner;
    }

    public void addOwnerAdress(InetAddress ownerAdress) {
        this.ownerAdress = ownerAdress;
    }
}
