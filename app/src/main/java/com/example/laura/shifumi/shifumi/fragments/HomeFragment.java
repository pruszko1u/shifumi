package com.example.laura.shifumi.shifumi.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.laura.shifumi.R;
import com.example.laura.shifumi.shifumi.listeners.ButtonListener;

/**
 * Created by Laura on 13/01/2018.
 */

public class HomeFragment extends Fragment {

    FragmentSelecteur fragmentSelecteur;

    public Button hostButton;
    public Button joinButton;

    public ButtonListener buttonListener;

    public HomeFragment() {   }

    @Override
    public void onStart()
    {
        super.onStart();

        buttonListener = new ButtonListener(fragmentSelecteur);

        joinButton = (Button)this.getActivity().findViewById(R.id.joinButton);

        if (joinButton != null)
            joinButton.setOnClickListener(buttonListener);

        if (hostButton != null)
            hostButton.setOnClickListener(buttonListener);

        this.fragmentSelecteur.joinButton = joinButton;
    }

    public void setFragmentSelecteur(FragmentSelecteur fragmentSelecteur) {
        this.fragmentSelecteur = fragmentSelecteur;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate( R.layout.home_fragment, container, false);
    }

}
