package com.example.laura.shifumi.connexionP2P;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.laura.shifumi.R;
import com.example.laura.shifumi.shifumi.fragments.JoinFragment;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class InitClientWifiP2PActivity extends AppCompatActivity implements WifiP2pManager.ChannelListener {

    WifiP2pManager manager;
    WifiP2pManager.Channel channel;
    InetAddress serverAdress;

    boolean isGroupOwner;
    public InetAddress ownerAdress;

    private final IntentFilter intentFilter = new IntentFilter();

    boolean isWifiP2pEnabled = false;

    Collection<WifiP2pDevice> listeAppareilsAProximite;
    String[] nomsAppareilsAProximite;

    PeersChangedListener peersChangedListener;
    MyPeerListener myPeerListener;
    ConnectionChangedListener connectionChangedListener;
    ConnectionInfoListener connectionInfoListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_init_client_wifi_p2_p);

        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);


        MyPeerListener peerListener = new MyPeerListener(this);

        manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d("ClientInit", "Success!");
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.d("ClientInit", "Failure!");
            }
        });

        this.updateList();

    }

    public void updateList () {

        Log.d("ClientInit", "UpdateList!");

        ListView listView = (ListView)this.findViewById(R.id.hostList);
        List<String> list;
        if (this.nomsAppareilsAProximite != null && this.nomsAppareilsAProximite.length > 0) {
            list = new ArrayList<String>(Arrays.asList(this.nomsAppareilsAProximite));
        } else {
            list = new ArrayList<String>();
            list.add("Aucun appareil à proximité");
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final String deviceName = ((TextView)view).getText().toString();

                if (deviceName != "Aucun appareil à proximité") {
                    WifiP2pDevice device = null;

                    for (WifiP2pDevice d : listeAppareilsAProximite) {
                        if (d.deviceName == deviceName)
                            device = d;
                    }

                    if (device != null) {
                        WifiP2pConfig config = new WifiP2pConfig();
                        config.deviceAddress = ((WifiP2pDevice)device).deviceAddress;
                        manager.connect(channel, config, new WifiP2pManager.ActionListener() {

                            @Override
                            public void onSuccess() {
                                Log.d("ClientInit onclick", "Success!");
                            }

                            @Override
                            public void onFailure(int reason) {
                                Log.d("ClientInit onclick", "Failure!");
                            }
                        });
                    }

                }
            }
        });

    }

    public void closeInitClient(WifiP2pDevice device) {
        Intent result = new Intent(this, JoinFragment.class);
        result.putExtra("result", device);
        result.putExtra("isGroupOwner", this.isGroupOwner);
        result.putExtra("groupOwnerAdress", this.ownerAdress);
        Log.d("InitClient", "groupOwnerAddress " + this.ownerAdress);
        setResult(Activity.RESULT_OK, result);


        Log.d("InitClient", "About to finish");

        finish();
    }


    /** register the BroadcastReceiver with the intent values to be matched */
    @Override
    public void onResume() {
        super.onResume();
        connectionChangedListener = new ConnectionChangedListener(manager, channel, this);
        connectionInfoListener = new ConnectionInfoListener(this);
        registerReceiver(connectionChangedListener, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(connectionChangedListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;
    }

    @Override
    public void onChannelDisconnected() {

    }

    public void setOwnerAdress(InetAddress ownerAdress) {
        Log.d("InitClient", "SetOwnerAddress " + ownerAdress.toString());
        this.ownerAdress = ownerAdress;
    }
}
