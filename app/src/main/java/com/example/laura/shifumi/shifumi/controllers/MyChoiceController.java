package com.example.laura.shifumi.shifumi.controllers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.laura.shifumi.shifumi.fragments.ShifumiFragment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;

/**
 * Created by Laura on 24/01/2018.
 */

public class MyChoiceController {

    public static ShifumiFragment shifumi;
    public static String myChoice;
    public static boolean iHaveChosen;
    public boolean initDone = false;
    SendChoiceController choiceController;

    Thread choice;

    public static final int SERVERPORT = 8888;

    public MyChoiceController (ShifumiFragment shifumi, InetAddress ownerAdress) {
        this.shifumi = shifumi;
        this.iHaveChosen = false;
        this.myChoice = "";

        choiceController = new SendChoiceController(this);
    }

    public void setMyChoice (String myChoice) {
        this.myChoice = myChoice;
        this.transferChoice(myChoice);
    }

    public void transferChoice(String action) {
        choiceController.setAction(action);
        choice = new Thread(choiceController);
        choice.start();
        if (this.myChoice.equals("scissors") || this.myChoice.equals("rock") || this.myChoice.equals("paper")) {
            this.iHaveChosen = true;
            shifumi.choiceDone();
        }
    }

    public void choiceTransfered() {
        try {
            choice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
