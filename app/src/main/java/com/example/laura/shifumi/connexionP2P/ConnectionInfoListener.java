package com.example.laura.shifumi.connexionP2P;

import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

/**
 * Created by Laura on 13/01/2018.
 */

public class ConnectionInfoListener implements WifiP2pManager.ConnectionInfoListener {

    InitClientWifiP2PActivity activity;
    WifiP2pDevice device;

    ConnectionInfoListener(InitClientWifiP2PActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo info) {
        //InetAddress groupOwnerAddress = info.groupOwnerAddress.getHostAddress();

        if (info.groupFormed && info.isGroupOwner) {
            this.activity.isGroupOwner = true;
            this.activity.closeInitClient(device);
            Log.d("ConnectionInfoListener", "Group is formed and groupOwner");
        } else if (info.groupFormed) {
            this.activity.isGroupOwner = false;
            this.activity.setOwnerAdress(info.groupOwnerAddress);
            this.activity.closeInitClient(device);
            Log.d("ConnectionInfoListener", "Group is formed and NOT groupOwner");
            Log.d("ConnectionInfoListener", "Group Owner : " + info.toString());
        }

    }

    public void setDevice(WifiP2pDevice device) {
        this.device = device;
    }
}
