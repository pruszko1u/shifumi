package com.example.laura.shifumi.shifumi.listeners;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.laura.shifumi.shifumi.controllers.MyChoiceController;
import com.example.laura.shifumi.shifumi.fragments.FragmentSelecteur;

/**
 * Created by Laura on 24/01/2018.
 */

public class PierrePapierCiseauxListener implements Button.OnClickListener {

    FragmentSelecteur selecteur;
    MyChoiceController myChoiceController;

    public PierrePapierCiseauxListener(FragmentSelecteur selecteur, MyChoiceController myChoiceController)
    {
        this.selecteur = selecteur;
        this.myChoiceController = myChoiceController;

    }
    @Override
    public void onClick(View v) {

        Fragment newFragment;
        FragmentManager fragmentManager = this.selecteur.getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (v == this.selecteur.paper) {
            myChoiceController.setMyChoice("paper");
        } else if (v == this.selecteur.scissors) {
            myChoiceController.setMyChoice("scissors");
        } else if (v == this.selecteur.rock) {
            myChoiceController.setMyChoice("rock");
        }

    }
}
