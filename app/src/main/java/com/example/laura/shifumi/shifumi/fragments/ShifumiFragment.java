package com.example.laura.shifumi.shifumi.fragments;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.net.wifi.p2p.WifiP2pDevice;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.laura.shifumi.R;
import com.example.laura.shifumi.shifumi.controllers.MyChoiceController;
import com.example.laura.shifumi.shifumi.controllers.OpponentChoiceController;
import com.example.laura.shifumi.shifumi.listeners.ButtonListener;
import com.example.laura.shifumi.shifumi.listeners.PierrePapierCiseauxListener;
import com.example.laura.shifumi.shifumi.model.GameModel;

import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;

import static com.example.laura.shifumi.R.*;

/**
 * Created by Laura on 23/01/2018.
 */

public class ShifumiFragment extends Fragment {

    public OpponentChoiceController opponentChoiceController;
    public MyChoiceController myChoice;

    public PierrePapierCiseauxListener pierrePapierCiseauxListener;
    public FragmentSelecteur fragmentSelecteur;

    public ImageView scissors;
    public ImageView paper;
    public ImageView rock;

    public GameModel gameModel;

    public ShifumiFragment(){}

    @Override
    public void onStart()
    {
        super.onStart();

        scissors = (ImageView)this.getActivity().findViewById(id.scissors);
        paper = (ImageView)this.getActivity().findViewById(id.paper);
        rock = (ImageView)this.getActivity().findViewById(id.rock);

        TextView victoires = (TextView)this.getActivity().findViewById(id.victory);
        TextView defaites = (TextView)this.getActivity().findViewById(id.defeat);
        victoires.setText(victoires.getText() + " " + this.gameModel.victoires);
        defaites.setText(defaites.getText() + " " + this.gameModel.defaites);

        TextView gameTitle = (TextView)this.getActivity().findViewById(id.gameTitle);
        gameTitle.setText("Round n°" + gameModel.round + " : " + gameTitle.getText());

        this.fragmentSelecteur.scissors = scissors;
        this.fragmentSelecteur.paper = paper;
        this.fragmentSelecteur.rock = rock;

        myChoice = new MyChoiceController(this, this.gameModel.getOwnerAdress());

        pierrePapierCiseauxListener = new PierrePapierCiseauxListener(fragmentSelecteur, myChoice);

        if (scissors != null)
            scissors.setOnClickListener(pierrePapierCiseauxListener);

        if (paper != null)
            paper.setOnClickListener(pierrePapierCiseauxListener);

        if (rock != null)
            rock.setOnClickListener(pierrePapierCiseauxListener);

        WindowManager wm = (WindowManager)this.getActivity().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        rock.getLayoutParams().height = height / 5;
        rock.getLayoutParams().width = height / 5;

        paper.getLayoutParams().height = height / 5;
        paper.getLayoutParams().width = height / 5;

        scissors.getLayoutParams().height = height / 5;
        scissors.getLayoutParams().width = height / 5;

        LinearLayout cardLayout = (LinearLayout)this.getActivity().findViewById(id.cardLayout);
        gameTitle = (TextView)this.getActivity().findViewById(id.gameTitle);
        gameTitle.setText("En attente de l'opposant");
        cardLayout.setVisibility(LinearLayout.GONE);
    }

    public void setFragmentSelecteur(FragmentSelecteur fragmentSelecteur) {
        this.fragmentSelecteur = fragmentSelecteur;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate( R.layout.fragment_game, container, false);
    }

    public void attenteOpponentReady() {
        if (this.gameModel.getIsGroupOwner() && this.gameModel.getClientAddress() == null) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.attenteOpponentReady();
        } else if (!this.gameModel.getIsGroupOwner() && !myChoice.initDone) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            myChoice.transferChoice("init");
            this.attenteOpponentReady();
        } else {
            LinearLayout cardLayout = (LinearLayout)this.getActivity().findViewById(id.cardLayout);
            TextView gameTitle = (TextView)this.getActivity().findViewById(id.gameTitle);
            gameTitle.setText(string.gameTitle);
            cardLayout.setVisibility(LinearLayout.VISIBLE);
        }
    }

    public void opponentChose() {

        if (this.myChoice.iHaveChosen && this.opponentChoiceController.opponentHasChosen) {
            this.endRound();
        } else if (this.opponentChoiceController.opponentHasChosen) {
            TextView opposantStatus = (TextView)this.getActivity().findViewById(id.opposantStatus);
            opposantStatus.setText(string.opponentHasPlayed);
            opposantStatus.setTextColor(ContextCompat.getColor(this.getActivity().getApplicationContext(), R.color.orange));
        }
    }

    @Override
    public void onStop () {
        super.onStop();

        this.opponentChoiceController.cancel(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        opponentChoiceController = new OpponentChoiceController(this.getActivity().getApplicationContext(), this);
        opponentChoiceController.execute();

        attenteOpponentReady();
    }

    public void setGameModel(GameModel gameModel) {
        this.gameModel = gameModel;
    }

    public void choiceDone() {

        if (this.myChoice.iHaveChosen && this.opponentChoiceController.opponentHasChosen) {
            this.endRound();
        } else if (this.myChoice.iHaveChosen) {
            LinearLayout cardLayout = (LinearLayout)this.getActivity().findViewById(id.cardLayout);
            TextView gameTitle = (TextView)this.getActivity().findViewById(id.gameTitle);
            gameTitle.setText(string.waitingOpponentChoice);
            cardLayout.setVisibility(LinearLayout.GONE);
        }
    }

    private void endRound() {
        ResultFragment results = new ResultFragment();
        results.setFragmentSelecteur(this.fragmentSelecteur);
        results.setOpponentChoiceController(opponentChoiceController);
        results.setMyChoiceController(myChoice);
        results.setGameModel(this.gameModel);
        this.fragmentSelecteur.replaceFragment(results);
    }
}
