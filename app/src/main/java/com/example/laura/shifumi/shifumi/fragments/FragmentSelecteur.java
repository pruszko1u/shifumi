package com.example.laura.shifumi.shifumi.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.laura.shifumi.R;
import com.example.laura.shifumi.shifumi.fragments.HomeFragment;
import com.example.laura.shifumi.shifumi.model.GameModel;

/**
 * Created by Laura on 13/01/2018.
 */

public class FragmentSelecteur extends Fragment {

    public Button joinButton;
    public Button launchGameButton;

    // images du jeu
    public ImageView scissors;
    public ImageView paper;
    public ImageView rock;
    public Button disconnectButton;
    public Button nextRoundButton;

    public boolean isStarted = false;
    public GameModel gameModel;


    public FragmentSelecteur() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_selecteur, container);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if (!isStarted) {
            HomeFragment homeFragment = new HomeFragment();
            homeFragment.setFragmentSelecteur(this);
            this.changeFragment(homeFragment);

            this.isStarted = true;
        }
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = this.getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentView, fragment);
        fragmentTransaction.commit();
    }

    public void changeFragment (Fragment fragment) {
        FragmentManager fragmentManager = this.getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentView, fragment);
        fragmentTransaction.commit();
    }

}
