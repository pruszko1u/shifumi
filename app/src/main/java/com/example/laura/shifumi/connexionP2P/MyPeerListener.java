package com.example.laura.shifumi.connexionP2P;

import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;

import java.util.Collection;

/**
 * Created by Laura on 13/01/2018.
 */

public class MyPeerListener implements WifiP2pManager.PeerListListener {

    InitClientWifiP2PActivity activity;

    public MyPeerListener(InitClientWifiP2PActivity activity) {
        this.activity = activity;
    }

    private static String[] filtreNoms(Collection<WifiP2pDevice> listeAppareilsAProximite)
    {
        String [] t = new String[listeAppareilsAProximite.size()];
        int i = -1;

        for (WifiP2pDevice d : listeAppareilsAProximite)
            t[++i] = d.deviceName;

        return t;
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peers) {

        Collection<WifiP2pDevice> refreshedPeers = peers.getDeviceList();

        if (!refreshedPeers.equals(peers)) {
                this.activity.listeAppareilsAProximite = refreshedPeers;

            this.activity.nomsAppareilsAProximite = filtreNoms(this.activity.listeAppareilsAProximite);
            this.activity.updateList();
        }
    }

    public void connectToGroupOwner(WifiP2pDevice groupOwner) {

        if (groupOwner != null) {
            WifiP2pConfig config = new WifiP2pConfig();
            config.wps.setup = WpsInfo.PBC;
            config.deviceAddress = groupOwner.deviceAddress;    // adresse MAC du Group Owner

            this.activity.manager.
                    connect(this.activity.channel, config, null); // tentative de connexion au serveur

        }
    }
}
