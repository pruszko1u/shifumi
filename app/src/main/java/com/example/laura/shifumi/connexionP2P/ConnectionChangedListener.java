package com.example.laura.shifumi.connexionP2P;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Laura on 13/01/2018.
 */

public class ConnectionChangedListener extends BroadcastReceiver {

    InitClientWifiP2PActivity activity;
    WifiP2pManager.Channel channel;
    WifiP2pManager manager;
    ConnectionInfoListener connectionListener;

    ConnectionChangedListener(WifiP2pManager manager, WifiP2pManager.Channel channel, InitClientWifiP2PActivity activity) {
        this.activity = activity;
        this.manager = manager;
        this.channel = channel;
        this.connectionListener = new ConnectionInfoListener(activity);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            Log.d("BroadcastReceiver", "WIFI_P2P_STATE_CHANGED_ACTION !");
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                activity.setIsWifiP2pEnabled(true);
            } else {
                activity.setIsWifiP2pEnabled(false);
            }
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

            Log.d("BroadcastReceiver", "WIFI_P2P_PEERS_CHANGED_ACTION !");

            if (manager != null) {
                manager.requestPeers(channel, new MyPeerListener(activity));
            }

        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {

            Log.d("BroadcastReceiver", "WIFI_P2P_CONNECTION_CHANGED_ACTION !");

            if (manager == null) {
                return;
            }

            NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            WifiP2pGroup groupInfos = (WifiP2pGroup) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_GROUP);

            if (networkInfo.isConnected()) {

                WifiP2pDevice device = null;

                if (!groupInfos.isGroupOwner()) {
                    device = groupInfos.getOwner();
                } else {
                    ArrayList<WifiP2pDevice> clientList = new ArrayList<>(groupInfos.getClientList());
                    if(clientList != null && clientList.size() > 0) {
                        device = clientList.get(0);
                    }
                }

                connectionListener.setDevice(device);

                manager.requestConnectionInfo(channel, connectionListener);
                Log.d("BroadcastReceiver", "Connected ");
            } else {
                Log.d("BroadcastReceiver", "NOT Connected !");
            }

        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            Log.d("BroadcastReceiver", "WIFI_P2P_CONNECTION_CHANGED_ACTION !");
        }
    }
}
