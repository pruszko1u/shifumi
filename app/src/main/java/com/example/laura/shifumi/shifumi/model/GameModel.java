package com.example.laura.shifumi.shifumi.model;

import android.net.wifi.p2p.WifiP2pDevice;

import java.net.InetAddress;

/**
 * Created by Laura on 28/01/2018.
 */

public class GameModel {

    public int round;
    public int victoires;
    public int defaites;

    public WifiP2pDevice opponentDevice;
    public boolean isGroupOwner;
    public InetAddress ownerAdress;
    public InetAddress clientAddress;

    public GameModel (WifiP2pDevice opponentDevice, boolean isGroupOwner, InetAddress ownerAdress) {
        this.round = 1;
        this.victoires = 0;
        this.defaites = 0;

        this.opponentDevice = opponentDevice;
        this.isGroupOwner = isGroupOwner;
        this.ownerAdress = ownerAdress;
    }

    public void gagne() {
        this.round++;
        this.victoires++;
    }

    public void defaite() {
        this.round++;
        this.defaites++;
    }

    public InetAddress getOwnerAdress() {
        return ownerAdress;
    }

    public void setOwnerAdress(InetAddress ownerAdress) {
        this.ownerAdress = ownerAdress;
    }

    public void setIsGroupOwner(boolean isGroupOwner) {
        this.isGroupOwner = isGroupOwner;
    }

    public boolean getIsGroupOwner() {
        return isGroupOwner;
    }

    public WifiP2pDevice getOpponentDevice() {
        return opponentDevice;
    }

    public void setOpponentDevice(WifiP2pDevice opponentDevice) {
        this.opponentDevice = opponentDevice;
    }

    public void setClientAddress(InetAddress clientAddress) {
        this.clientAddress = clientAddress;
    }

    public InetAddress getClientAddress() {
        return this.clientAddress;
    }
}
