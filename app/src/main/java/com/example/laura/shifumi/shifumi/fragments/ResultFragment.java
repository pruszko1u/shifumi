package com.example.laura.shifumi.shifumi.fragments;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.laura.shifumi.R;
import com.example.laura.shifumi.shifumi.controllers.MyChoiceController;
import com.example.laura.shifumi.shifumi.controllers.OpponentChoiceController;
import com.example.laura.shifumi.shifumi.listeners.ButtonListener;
import com.example.laura.shifumi.shifumi.model.GameModel;

/**
 * Created by Laura on 28/01/2018.
 */

public class ResultFragment extends Fragment {

    public FragmentSelecteur fragmentSelecteur;
    public ButtonListener buttonListener;

    public OpponentChoiceController opponentChoiceController;
    public MyChoiceController myChoiceController;
    private GameModel gameModel;

    public ResultFragment() {}

    @Override
    public void onStart() {
        super.onStart();

        buttonListener = new ButtonListener(fragmentSelecteur);

        ImageView playerImg = (ImageView) this.getActivity().findViewById(R.id.playerImg);
        ImageView opponentImg = (ImageView) this.getActivity().findViewById(R.id.opponentImg);

        TextView resultText = (TextView) this.getActivity().findViewById(R.id.resultText);

        Button nextRoundButton = (Button)this.getActivity().findViewById(R.id.nextRoundButton);

        fragmentSelecteur.nextRoundButton = nextRoundButton;

        if (nextRoundButton != null)
            nextRoundButton.setOnClickListener(buttonListener);

        int playerChoice = -1;
        int opponentChoice = -1;

        boolean victoire = false;

        switch (myChoiceController.myChoice) {
            case "paper":
                playerChoice = R.mipmap.paper;
                break;
            case "rock":
                playerChoice = R.mipmap.rock;
                break;
            case "scissors":
                playerChoice = R.mipmap.scissors;
                break;
        }

        switch (opponentChoiceController.opponentChoice) {
            case "paper":
                opponentChoice = R.mipmap.paper;
                if (myChoiceController.myChoice.equals("scissors"))
                    victoire = true;
                break;
            case "rock":
                opponentChoice = R.mipmap.rock;
                if (myChoiceController.myChoice.equals("paper"))
                    victoire = true;
                break;
            case "scissors":
                opponentChoice = R.mipmap.scissors;
                if (myChoiceController.myChoice.equals("rock"))
                    victoire = true;
                break;
        }

        if (victoire) {
            resultText.setText(R.string.roundGagne);
            gameModel.gagne();
        } else if (myChoiceController.myChoice.equals(opponentChoiceController.opponentChoice)) {
            resultText.setText(R.string.roundEgal);
        } else {
            resultText.setText(R.string.roundPerdu);
            gameModel.defaite();
        }

        WindowManager wm = (WindowManager)this.getActivity().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        playerImg.setImageResource(playerChoice);
        playerImg.getLayoutParams().height = height / 5;
        playerImg.getLayoutParams().width = height / 5;

        opponentImg.setImageResource(opponentChoice);
        opponentImg.getLayoutParams().height = height / 5;
        opponentImg.getLayoutParams().width = height / 5;
    }

    public void setGameModel(GameModel gameModel) {
        this.gameModel = gameModel;
    }

    public void setFragmentSelecteur(FragmentSelecteur fragmentSelecteur) {
        this.fragmentSelecteur = fragmentSelecteur;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate( R.layout.fragment_result_round, container, false);
    }

    public void setOpponentChoiceController(OpponentChoiceController opponentChoiceController) {
        this.opponentChoiceController = opponentChoiceController;
    }

    public void setMyChoiceController(MyChoiceController myChoiceController) {
        this.myChoiceController = myChoiceController;
    }
}
