package com.example.laura.shifumi.shifumi.controllers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.laura.shifumi.R;
import com.example.laura.shifumi.shifumi.fragments.ShifumiFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Laura on 23/01/2018.
 */

public class OpponentChoiceController extends AsyncTask <String, Void, String>{

    private Context context;
    public String opponentChoice;
    private ShifumiFragment mainGame;
    ServerSocket serverSocket = null;

    public boolean running;

    public boolean opponentHasChosen;

    public OpponentChoiceController(Context context, ShifumiFragment main) {
        this.context = context;
        this.mainGame = main;
        this.opponentChoice = "";
        opponentHasChosen = false;
        running = true;
    }

    @Override
    protected String doInBackground(String... params) {

        String choice = "";

        while(running) {

            try {
                serverSocket = new ServerSocket(8888);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                Socket client = serverSocket.accept();

                if (!choice.isEmpty())
                    choice = "";

                BufferedReader r = new BufferedReader(new InputStreamReader(client.getInputStream()));
                choice = r.readLine();

                if (choice.equals("init")) {
                    this.mainGame.gameModel.setClientAddress(client.getInetAddress());
                } else if (choice.equals("scissors") || choice.equals("rock") || choice.equals("paper")) {
                    running = false;
                }

                serverSocket.close();

                if (!running)
                    break;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return (choice != "" ? choice : null);
    }

    @Override
    protected void onPostExecute(String choice) {

        if (choice != null) {
            this.opponentHasChosen = true;
            this.opponentChoice = choice.toString();
            mainGame.opponentChose();
        }
    }
}
