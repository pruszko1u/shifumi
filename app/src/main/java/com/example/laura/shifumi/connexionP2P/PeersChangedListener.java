package com.example.laura.shifumi.connexionP2P;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;

/**
 * Created by Laura on 13/01/2018.
 */

public class PeersChangedListener extends BroadcastReceiver {

    InitClientWifiP2PActivity activity;

    PeersChangedListener(InitClientWifiP2PActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (action.equals(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION))
        {
            activity.manager.requestPeers(activity.channel, activity.myPeerListener);
        }

    }
}
