package com.example.laura.shifumi.shifumi.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.laura.shifumi.R;
import com.example.laura.shifumi.connexionP2P.InitClientWifiP2PActivity;
import com.example.laura.shifumi.shifumi.listeners.ButtonListener;

import java.net.InetAddress;

/**
 * Created by Laura on 13/01/2018.
 */

public class JoinFragment extends Fragment {

    static final int SEND_JOIN_REQUEST = 1;

    FragmentSelecteur fragmentSelecteur;
    ButtonListener buttonListener;
    TextView joinText;
    Button launchGameButton;
    Button disconnectButton;
    View rootView;
    Context context;

    public JoinFragment() {   }

    @Override
    public void onStart()
    {
        super.onStart();

        this.launchGameButton = (Button)this.getActivity().findViewById(R.id.launchGameButton);
        this.launchGameButton.setBackgroundResource(R.color.mediumGrey);

        this.disconnectButton = (Button)this.getActivity().findViewById(R.id.disconnectButton);
        this.disconnectButton.setBackgroundResource(R.color.mediumGrey);

        this.fragmentSelecteur.launchGameButton = this.launchGameButton;
        buttonListener = new ButtonListener(fragmentSelecteur);

        this.fragmentSelecteur.disconnectButton = this.disconnectButton;
        buttonListener = new ButtonListener(fragmentSelecteur);

        Intent intent = new Intent(context, InitClientWifiP2PActivity.class);
        startActivityForResult(intent, SEND_JOIN_REQUEST);
    }

    @Override
    public void startActivityForResult(@RequiresPermission Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SEND_JOIN_REQUEST) {
            Bundle bundle = data.getExtras();
            WifiP2pDevice result = (WifiP2pDevice) bundle.get("result");
            InetAddress ownerAdress = (InetAddress) bundle.get("groupOwnerAdress");
            boolean isGroupOwner = (boolean) bundle.get("isGroupOwner");

            if (result != null) {
                this.launchGameButton = (Button)this.getActivity().findViewById(R.id.launchGameButton);
                this.launchGameButton.setBackgroundResource(R.color.teal);

                this.disconnectButton = (Button)this.getActivity().findViewById(R.id.disconnectButton);
                this.disconnectButton.setBackgroundResource(R.color.orange);

                buttonListener.addOwnerAdress(ownerAdress);
                buttonListener.addIsGroupOwner(isGroupOwner);
                buttonListener.addOpponentDevice(result);

                if (this.launchGameButton != null)
                    this.launchGameButton.setOnClickListener(this.buttonListener);

                if (this.disconnectButton != null)
                    this.disconnectButton.setOnClickListener(this.buttonListener);

                this.joinText = (TextView)this.getActivity().findViewById(R.id.joinText);
                this.joinText.setText("Connecté à " + result.deviceName);
        }
        }
    }

    public void setFragmentSelecteur(FragmentSelecteur fragmentSelecteur) {
        this.fragmentSelecteur = fragmentSelecteur;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate( R.layout.fragment_join, container, false);
        context = rootView.getContext();

        return rootView;
    }
}
